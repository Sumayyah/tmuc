﻿using System;

namespace Practice
{
    class Program
    {
        static void Main(string[] args)
        {
            Creditcard cc = new TitaniumFactory().CreateProduct();
            if (cc != null)
            {
                Console.WriteLine("Card Type:" + cc.GetCardtype());
                Console.WriteLine("Limit:" + cc.GetLimit());
                Console.WriteLine("Annual Charge:" + cc.GetAnnualCharge());
            }
            else
            {
                Console.WriteLine("Invalid card type");
            }
            Console.WriteLine(".................");
            cc = new SilverFactory().CreateProduct();
            if (cc != null)
            {
                Console.WriteLine("Card Type:" + cc.GetCardtype());
                Console.WriteLine("Limit:" + cc.GetLimit());
                Console.WriteLine("Annual Charge:" + cc.GetAnnualCharge());
            }
            else
            {
                Console.WriteLine("Invalid card type");
            }
            Console.ReadLine();
        }
        public interface Creditcard
        {
            string GetCardtype();
            int GetLimit();
            int GetAnnualCharge();

        }
        public class Silver : Creditcard
        {
            public string GetCardtype()
            {
                return "Silver Signature";
            }
            public int GetLimit()
            {
                return 45000;
            }
            public int GetAnnualCharge()
            {
                return 3000;
            }
        }
        public class Titanium : Creditcard
        {
            public string GetCardtype()
            {
                return "Titanium Plus";
            }
            public int GetLimit()
            {
                return 35000;
            }
            public int GetAnnualCharge()
            {
                return 2000;
            }

        }
        public class Gold : Creditcard
        {
            public string GetCardtype()
            {
                return "Gold";
            }
            public int GetLimit()
            {
                return 25000;
            }
            public int GetAnnualCharge()
            {
                return 1500;
            }
        }
        public abstract class CreditCardFactory
        {
            protected abstract Creditcard MakeProduct();
            public Creditcard CreateProduct()
            {
                return this.MakeProduct();

            }
        }
        public class GoldFactory : CreditCardFactory
        {
            protected override Creditcard MakeProduct()
            {
                Creditcard product = new Gold();
                return product;
            }

        }
        public class TitaniumFactory : CreditCardFactory
        {
            protected override Creditcard MakeProduct()
            {
                Creditcard product = new Titanium();
                return product;
            }
        }
        public class SilverFactory : CreditCardFactory
        {
            protected override Creditcard MakeProduct()
            {
                Creditcard product = new Silver();
                return product;
            }
        }
    }
}
